const TELEGRAM_TOKEN = process.env.TELEGRAM_TOKEN;
const TELEGRAM_SUBSCRIBER = process.env.TELEGRAM_SUBSCRIBER;
const PORT = process.env.PORT || 5001;

const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const Telegram = require('telegraf/telegram')
const telegram = new Telegram(TELEGRAM_TOKEN);

const moment = require("moment");
const momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/debug', (req, res) => {
    console.log(req.url);
    console.log(req.body);

    telegram
        .sendMessage(TELEGRAM_SUBSCRIBER, req.body)
        .then(data => res.send('ok'))
        .catch(err => res.status(500).send(err))
});

const allowed_labels = [
    'alertname',
    'severity',
    'job',
    'instance',
    'kubernetes_name',
    'kubernetes_namespace'
];

app.use('/prom', (req, res) => {
    try {
        let messages = new Set();
        const block = '```';

        const formatMessage = (name, value) => {
            return `\n*${name}*:${block} ${value} ${block}`;
        }

        req.body.alerts.forEach(alert => {
            let message = `*status*: ${alert.status.toUpperCase()}`;

            if (alert.annotations) {
                Object.keys(alert.annotations).forEach(name => {
                    message += formatMessage(name, alert.annotations[name]);
                })
            }

            const start = moment(alert.startsAt);
            const end = moment(alert.endsAt);
            const diff = end.diff(start, "seconds");

            if (diff > 0) {
                const duration = moment.duration(diff, "seconds").format("d[d] h[h] m[m] s[s]", {
                    trim: "both"
                });
                message += formatMessage("duration", duration);
            }

            if (alert.labels) {
                Object.keys(alert.labels).forEach(name => {
                    if (allowed_labels.indexOf(name) >= 0) {
                        message += formatMessage(name, alert.labels[name]);
                    }
                })
            }

            messages.add(message);
        });

        messages.forEach(message => {
            telegram
                .sendMessage(TELEGRAM_SUBSCRIBER, message, { parse_mode: "Markdown" })
                .catch(err => console.error(err))
        });

        res.send('ok');

    } catch (err) {
        console.error(err);

        telegram
            .sendMessage(TELEGRAM_SUBSCRIBER, req.body)
            .then(data => res.send('ok'))
            .catch(err => res.status(500).send(err))
    }
});

app.use('/send', (req, res) => {
    if (!req.query.text) {
        res.status(500).send('no text');
        return;
    }
    telegram
        .sendMessage(TELEGRAM_SUBSCRIBER, req.query.text)
        .catch(err => res.status(500).send(err))
});

app.listen(PORT, function () {
    console.log(`started on port ${PORT}`);
});